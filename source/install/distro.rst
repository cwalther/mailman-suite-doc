.. _distro-install:

=============================================
Installing Mailman from Linux distro packages
=============================================

Other distribution specific packages for Mailman 3 are not yet available. If you
would like to help package Mailman 3 for your favorite Linux distro, please get
in touch at mailman-developers@python.org.


.. _debian-install:

Installing on Debian
--------------------

Debian provides Mailman 3 packages. The meta-package ``mailman3-full`` depends on
all components of a complete Mailman 3 suite. If you want to split the
installation of the Mailman 3 core mailinglist delivery daemon and the Mailman 3
Django web suite with Postorius and Hyperkitty, take a look at the packages
``mailman3`` and ``mailman3-web``.

The Debian Mailman 3 packages can be found here:

    https://packages.debian.org/search?keywords=mailman3
